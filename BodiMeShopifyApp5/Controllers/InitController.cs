﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using BodiMeShopifyApp5.Infrastructure;
using BodiMeShopifyApp5.Services;
using BodiMeShopifyApp5.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using OpenIddict.Validation.AspNetCore;

namespace BodiMeShopifyApp5.Controllers
{
    [Route("/[Controller]")]
    [ApiController]

    public class InitController : Controller
    {

        private readonly ILoggerManager _logger;
        private readonly IInitService _initService;

        public InitController(ILoggerManager logger, IInitService initService)
        {
            _logger = logger;
            _initService = initService;
        }

        //CREATE CREDENTIALS
        //ToDo Add Update
        [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
        [ProducesResponseType(200)] //successful update
        [ProducesResponseType(201)] //successful new creation
        [ProducesResponseType(404)] //failed update 
        [ProducesResponseType(400)] //database save failed
        [HttpPost("credentials", Name = nameof(CreateUpdateBodiMeCredentials))]
        public async Task<IActionResult> CreateUpdateBodiMeCredentials([FromBody] JObject credentialJSON)
        {
            _logger.LogInfo("InitController_CreateUpdateBodiMeCredentials_credentialData_" + credentialJSON);

            // dynamic input from inbound JSON
            dynamic credentialData = credentialJSON;

            var created = await _initService.CreateUpdateBodiMeCredentialsAsync(credentialData);
            if (created < 1) return BadRequest(new ApiError(_logger).ApiErrorMsg(Constant.CredentialCreationFailure));

            return Ok();
        }

        //CREATE SHOP TOKEN
        //ToDo Add Update
        [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
        [ProducesResponseType(200)] //successful update
        [ProducesResponseType(201)] //successful new creation
        [ProducesResponseType(404)] //failed update 
        [ProducesResponseType(400)] //database save failed
        [HttpPost("shoptoken", Name = nameof(CreateUpdateShopToken))]
        public async Task<IActionResult> CreateUpdateShopToken([FromBody] JObject tokenJSON)
        {
            _logger.LogInfo("InitController_CreateUpdateShopToken_tokenJSON_" + tokenJSON);

            // dynamic input from inbound JSON
            dynamic tokenData = tokenJSON;

            var created = await _initService.CreateUpdateShopTokenAsync(tokenData);
            if (created < 1) return BadRequest(new ApiError(_logger).ApiErrorMsg(Constant.ShopKeyCreationFailure));

            return Ok();
        }

    }
}
