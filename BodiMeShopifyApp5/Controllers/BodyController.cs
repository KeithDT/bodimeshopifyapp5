﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using BodiMeShopifyApp5.Services;
using Microsoft.AspNetCore.Mvc;

namespace BodiMeShopifyApp5.Controllers
{
    [Route("shopify/customers")]
    public class BodyController : Controller
    {

        private readonly IBodiMeCoreService _bodiMeCoreService;

        public BodyController(IBodiMeCoreService bodiMeCoreService)
        {
            _bodiMeCoreService = bodiMeCoreService;
        }

        //GET AN INDIVIDUAL BODY WITH ITS MEASUREMENTS
        [HttpGet]
        [Route("{customerId}")]
        public async Task<ActionResult> GetBodyMeasurements(string customerId, string shop)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.GetBodyMeasurements(customerId, shop);
                
                var res = await response.Content.ReadAsStringAsync();
                res = res.Replace(@"\", string.Empty);
                return new ContentResult { Content = res, StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //GET BODY PROFILE
        //returns the elements body measurements were created from
        [HttpGet]
        [Route("profile/{customerId}")]
        public async Task<ActionResult> GetBodyProfile(string customerId, string shop)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.GetBodyProfile(customerId, shop);

                var res = await response.Content.ReadAsStringAsync();
                //removed as was replacing & in brand names with ASCII code
                //res = res.Replace(@"\", string.Empty);
                return new ContentResult { Content = res, StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //DELETE A COLLECTION OF ELEMENTS FROM BODY PROFILE
        [HttpPost]
        [Route("body/profile/{customerId}/delete")]
        public async Task<ActionResult> DeleteBodyProfile([FromBody]Models.BodyProfileDeleteCollection profiles,string customerId, string shop)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.DeleteBodyProfile(profiles, customerId, shop);

                var res = await response.Content.ReadAsStringAsync();
                res = res.Replace(@"\", string.Empty);
                return new ContentResult { Content = res, StatusCode = (int)response.StatusCode, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //CREATE A BODY
        [HttpPost]
        [Route("body")]
        public async Task<ActionResult> CreateBody([FromBody] Models.BodyBrandMeasurement bodyBrandMeasurements, string shop)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.CreateUpdateBody(bodyBrandMeasurements, shop);
                
                return new ContentResult { Content = await response.Content.ReadAsStringAsync(), StatusCode = (int)response.StatusCode, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }
    }
}
