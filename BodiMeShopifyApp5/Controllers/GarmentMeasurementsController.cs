﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using BodiMeShopifyApp5.Infrastructure;
using BodiMeShopifyApp5.Services;
using BodiMeShopifyApp5.Settings;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace BodiMeShopifyApp5.Controllers
{
    [Route("shopify/garments")]
    public class GarmentMeasurementsController : Controller
    {
        private readonly IBodiMeCoreService _bodiMeCoreService;
        private readonly ILoggerManager _logger;
        public GarmentMeasurementsController(IBodiMeCoreService bodiMeCoreService, ILoggerManager logger)
        {
            _bodiMeCoreService = bodiMeCoreService;
            _logger = logger;
        }

        //GET SIZE CHARTS SUPPORTED BY THIS CLIENT
        [HttpGet]
        [Route("client/sizecharts")]
        [Route("client/sizecharts/{garmentId}")]
        public async Task<ActionResult> GetSizeCharts(string shop, string garmentId=null)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.GetSizeCharts(shop,garmentId);

                return new ContentResult { Content = await response.Content.ReadAsStringAsync(), StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //GET BRANDS FOR THIS CLIENT BY GENDER
        [HttpGet]
        [Route("client/brands/{gender}")]
        //Gender 0=Male 1=Female
        public async Task<ActionResult> GetBrandsByGender(string shop, int gender)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.GetBrandsByGender(shop, gender);

                return new ContentResult { Content = await response.Content.ReadAsStringAsync(), StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //GET CATEGORIES FOR THIS CLIENT, BRAND AND GENDER
        [HttpGet]
        [Route("client/brandcategories/{brand_id}/{gender}")]
        //Gender 0=Male 1=Female
        public async Task<ActionResult> GetBrandCategoriesByGender(string shop, int brand_id, int gender)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.GetBrandCategoriesByGender(shop,brand_id, gender);

                return new ContentResult { Content = await response.Content.ReadAsStringAsync(), StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //GET SIZES FOR BRAND CATEGORY
        [HttpGet]
        [Route("client/brandcategorysizes/{brand_id}/{category_id}")]
        public async Task<ActionResult> GetBrandSizesByCategory(string shop, int brand_id, int category_id)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.GetBrandSizesByCategory(shop, brand_id, category_id);

                return new ContentResult { Content = await response.Content.ReadAsStringAsync(), StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //GET DEFAULT UNITS BY GETTING CLIENT WHICH CONTAINS THEM
        [HttpGet]
        [Route("client/defaultUnits")]
        public async Task<ActionResult> GetDefaultUnits(string shop)
        {
            try
            {
                _logger.LogInfo("GarmentMeasurementsController_GetDefaultUnits_Start");

                HttpResponseMessage response = await _bodiMeCoreService.GetDefaultUnits(shop);

                var res = await response.Content.ReadAsStringAsync();
                res = res.Replace(@"\", string.Empty);

                _logger.LogInfo("GarmentMeasurementsController_GetDefaultUnits_End");

                return new ContentResult { Content = res, StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //CHECK GARMENT SIZE ADVICE IS ACTIVE
        [HttpGet]
        [Route("advice/{garmentId}")]
        public async Task<ActionResult> GetGarmentAdvice(string garmentId, string shop)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.GetGarmentInformation($"garment/advice/{garmentId}", shop);

                var adviceEnabled = response.StatusCode == HttpStatusCode.NoContent ? bool.FalseString.ToLower() : bool.TrueString.ToLower();
                return new ContentResult { Content = adviceEnabled, StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //GET ALL BODY MANDATORY MEASUREMENTS TO BE ASKED FOR FOR A GARMENT, PLUS ANY ADDITIONAL ONES
        [HttpGet]
        [Route("measurements/{garmentId}")]
        public async Task<ActionResult> GetGarmentMeasurements(string garmentId, string shop)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.GetGarmentInformation($"garment/measurements/{garmentId}", shop);

                return new ContentResult { Content = await response.Content.ReadAsStringAsync(), StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //CREATE OR UPDATE A BODY WITH GENERIC SIZE CHART DATA AND CALL SIZE ADVICE FOR THE BODY AND THE GARMENT
        [HttpPost]
        [Route("recommendedSize")]
        public async Task<ActionResult> GetRecommendedSize([FromBody]Dictionary<string, string> bodyMeasurementsMap, string customerId, string garmentId, string shop)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.CreateUpdateBodyWithGenericSizeChartAndReturnSizeAdvice(bodyMeasurementsMap, customerId, garmentId, shop);

                return new ContentResult { Content = await response.Content.ReadAsStringAsync(), StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //GET SIZE ADVICE FOR AN EXISTING BODY AND GARMENT
        [HttpGet]
        [Route("recommendedSizeOnly")]
        public async Task<ActionResult> GetRecommendedBrandSize(string customerId, string garmentId, string shop)
        {
            try
            {
                HttpResponseMessage response = await _bodiMeCoreService.GetRecommendedSize(customerId, garmentId, shop);

                return new ContentResult { Content = await response.Content.ReadAsStringAsync(), StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        //UPDATES BODY MEASUREMENTS
        [HttpPut]
        [Route("saveMeasurements")]
        public async Task<ActionResult> SaveProfile([FromBody]Dictionary<string, string> bodyMeasurementsMap, string customerId, string shop)
        {
            try
            {
                HttpResponseMessage  response = await _bodiMeCoreService.SaveProfileMeasurements(bodyMeasurementsMap, customerId, shop);
                
                return new ContentResult { Content = await response.Content.ReadAsStringAsync(), StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Request Failed. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }
    }
}
