﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BodiMeShopifyApp5.Infrastructure;
using BodiMeShopifyApp5.Services;
using BodiMeShopifyApp5.Settings;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ShopifySharp;
using ShopifySharp.Enums;

namespace BodiMeShopifyApp5.Controllers
{
    [Route("shopify")]
    public class ShopifyController : Controller
    {
        private readonly IAppDbService _appDbService;
        private readonly ShopifySetting _shopifySetting;
        private readonly ILoggerManager _logger;

        public ShopifyController(IOptions<AppSetting> appSettings, IAppDbService appDbService, ILoggerManager logger)
        {
            _shopifySetting = appSettings.Value.Shopify;
            _appDbService = appDbService;
            _logger = logger;
        }

        [HttpGet]
        [Route("handshake")]
        public async Task<ActionResult> Handshake(string shop)
        {
            _logger.LogInfo("ShopifyController_scripts_connect_Handshake:" + shop);

            if (!await AuthorizationService.IsValidShopDomainAsync(shop))
            {
                ModelState.AddModelError("", "The URL you entered is not a valid *.myshopify.com URL.");
                //Preserve the user's shopUrl so they don't have to type it in again.
                return new ContentResult{Content = "Handshake bad", StatusCode = 500};
            }
            return new RedirectResult($"connect/{shop}");
        }

        [HttpGet]
        [Route("connect/{shopUrl}")]
        public async Task<ActionResult> Connect(string shopUrl)
        {
            _logger.LogInfo("ShopifyController_scripts_connect_Shop:" + shopUrl);

            if (!await AuthorizationService.IsValidShopDomainAsync(shopUrl))
            {
                return new BadRequestObjectResult("The URL you entered is not a valid *.myshopify.com URL.");
            }
            //Determine the permissions that your app will need and request them here.
            var permissions = new List<AuthorizationScope> { AuthorizationScope.ReadProducts, AuthorizationScope.ReadScriptTags, AuthorizationScope.ReadThemes, AuthorizationScope.WriteScriptTags, AuthorizationScope.ReadCustomers };
            //Build the authorization URL
            var authUrl = AuthorizationService.BuildAuthorizationUrl(permissions, shopUrl, _shopifySetting.SiteKey, _shopifySetting.RedirectUrl);
            //Build the authorization URL and send the user to it
            return Redirect(authUrl.ToString());
        }

        [HttpGet]
        [Route("authresult")]
        public async Task<ActionResult> AuthorizeAndGenerateToken()
        {
            _logger.LogInfo("ShopifyController_scripts_authresult:");

            var queryParams = ControllerContext.HttpContext.Request.Query;
            queryParams.TryGetValue("code", out var code);
            queryParams.TryGetValue("shop", out var shop);
            var accessToken = await AuthorizationService.Authorize(code, shop, _shopifySetting.SiteKey, _shopifySetting.ApiSecret);
            await _appDbService.CreateShopAccessToken(accessToken, shop);
            //await InstallScript(shop, _shopifySetting.BodiScriptToInstallOnShop);
            return Redirect($"https://{shop}/admin/apps");
        }

        [HttpGet]
        [Route("scripts/install")]
        public async Task<ActionResult> InstallShopifyScript(string shop, string scriptLocation)
        {
            _logger.LogInfo("ShopifyController_scripts_install_Shop:" + shop + "::scriptlocation" + scriptLocation);

            try
            {
                var scriptCreated = await InstallScript(shop, scriptLocation);
                return new ContentResult { Content = JsonConvert.SerializeObject(scriptCreated), StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                _logger.LogError("ShopifyController_scripts_install_exception:" +  e.ToString());

                return new ContentResult { Content = $"Failed to install script. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        [HttpGet]
        [Route("scripts/uninstall")]
        public async Task<ActionResult> UninstallScript(string shop, long tagId)
        {
            _logger.LogInfo("ShopifyController_scripts_uninstall_Shop:" + shop + "::tagId" + tagId);

            try
            {
                await RemoveScript(shop, tagId);
                return new ContentResult { Content = JsonConvert.SerializeObject("Deleted"), StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                _logger.LogError("ShopifyController_scripts_uninstall_exception:" + e.ToString());

                return new ContentResult { Content = $"Failed to uninstall script. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        [HttpGet]
        [Route("scripts")]
        public async Task<ActionResult> GetScripts(string shop)
        {
            _logger.LogInfo("ShopifyController_scripts_uninstall_Shop:" + shop);

            try
            {
                var shopAccessToken = await _appDbService.GetShopAccessToken(shop);
                var scriptTagService = new ScriptTagService(shop, shopAccessToken);
                var scriptsList = scriptTagService.ListAsync();
                //added ignore reference loop checking as this was generating Self referencing loop detected for property 'Task' error
                var json = JsonConvert.SerializeObject(scriptsList,
                    new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                return new ContentResult { Content = json, StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                _logger.LogError("ShopifyController_scripts_getscripts_exception:" + e.ToString());

                return new ContentResult { Content = $"Failed to get scripts. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }

        private async Task<ScriptTag> InstallScript(string shop, string scriptLocation)
        {
            var shopAccessToken = await _appDbService.GetShopAccessToken(shop);
            _logger.LogInfo("ShopifyController_InstallScript_shopAccessToken:" + shopAccessToken);
            var scriptTagId = await _appDbService.GetScriptTagId(shop);
            _logger.LogInfo("ShopifyController_InstallScript_scriptTagId:" + scriptTagId);
            if (!string.IsNullOrEmpty(scriptTagId))
            {
                await RemoveScript(shop, Convert.ToInt64(scriptTagId));
            }
            var scriptTagService = new ScriptTagService(shop, shopAccessToken);
            var scriptTag = new ScriptTag { Event = "onload", Src = scriptLocation };
            var scriptCreated = await scriptTagService.CreateAsync(scriptTag);
            await _appDbService.AddScriptTag(shop, scriptCreated.Id.ToString());
            return scriptCreated;
        }

        private async Task<bool> RemoveScript(string shop, long tagId)
        {
            _logger.LogInfo("ShopifyController_RemoveScript_scriptTagId:" + tagId);
            var shopAccessToken = await _appDbService.GetShopAccessToken(shop);
            _logger.LogInfo("ShopifyController_RemoveScript_shopAccessToken:" + shopAccessToken);
            var scriptTagService = new ScriptTagService(shop, shopAccessToken);
            await scriptTagService.DeleteAsync(tagId);

            //delete the tagId from the db otherwise the install fails
            //ToDo better error handling
            var deleted = await _appDbService.DeleteScriptTagId(shop);

            return true;
        }
    }
}
