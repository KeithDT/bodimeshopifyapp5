﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BodiMeShopifyApp5.Services;
using BodiMeShopifyApp5.Settings;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ShopifySharp;
using ShopifySharp.Filters;

namespace BodiMeShopifyApp5.Controllers
{
    [Route("shopify/products")]
    public class ShopifyProductController : Controller
    {
        private readonly IAppDbService _appDbService;
        private readonly IBodiMeCoreService _bodiMeCoreService;

        public ShopifyProductController(IAppDbService appDbService, IBodiMeCoreService bodiMeCoreService)
        {
            _appDbService = appDbService;
            _bodiMeCoreService = bodiMeCoreService;
        }

        [HttpGet]
        [Route("sync/display")]
        [Route("{environment}_sync/display")]
        public async Task<ActionResult> DisplaySyncPage(string shop, [FromQuery] int page = 1)
        {
            var shopAccessToken = await _appDbService.GetShopAccessToken(shop);
            int productCount = await _bodiMeCoreService.GetProductCount(shopAccessToken, shop);

            var viewObject = new Tuple<string, int>(shop, productCount);
            return View("Index", viewObject);
        }

        [HttpGet]
        [Route("sync")]
        [Route("{environment}_sync")]
        public async Task<ActionResult> SyncProducts(string shop, [FromQuery] int page = 1)
        {
            var shopAccessToken = await _appDbService.GetShopAccessToken(shop);
            try
            {
                int productsSyncedCount = await _bodiMeCoreService.SyncProducts(shopAccessToken, shop);
                string add_message = "Synced on Prod environment";

                var output = JsonConvert.SerializeObject(new { message = $"Sync success. {productsSyncedCount} products synced. ({add_message})" });
                return new ContentResult { Content = output, StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                var output = JsonConvert.SerializeObject(new { message = $"Sync failed. {e}" });
                return new ContentResult { Content = output, StatusCode = 500, ContentType = "application/json" };
            }
        }

        [HttpGet]
        [Route("{productHandle}")]
        public async Task<ActionResult> GetProduct(string shop, string productHandle)
        {
            var shopAccessToken = await _appDbService.GetShopAccessToken(shop);
            try
            {
                var productService = new ProductService(shop, shopAccessToken);
                var products = await productService.ListAsync(new ProductListFilter { Handle = productHandle});
                var output = JsonConvert.SerializeObject(products.Items.First());
                return new ContentResult { Content = output, StatusCode = 200, ContentType = "application/json" };
            }
            catch (Exception e)
            {
                return new ContentResult { Content = $"Failed to get product by its handle - {productHandle}. {e}", StatusCode = 500, ContentType = "application/json" };
            }
        }
    }
}

