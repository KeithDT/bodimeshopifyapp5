﻿namespace BodiMeShopifyApp5.Settings
{
    public class AppSetting
    {
        public ShopifySetting Shopify { get; set; }

        public EnvironmentsSetting Environments { get; set; }
    }
}
