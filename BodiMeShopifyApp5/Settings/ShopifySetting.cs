﻿namespace BodiMeShopifyApp5.Settings
{
    public class ShopifySetting
    {
        public string SiteKey { get; set; }

        public string ApiSecret { get; set; }

        public string RedirectUrl { get; set; }

    }
}
