﻿namespace BodiMeShopifyApp5.Settings
{
    public class EnvironmentsSetting
    {
        public string Production { get; set; }

        public string Development { get; set; }
    }
}
