﻿using System.Threading.Tasks;
using BodiMeShopifyApp5.Models;

namespace BodiMeShopifyApp5.Services
{
    public interface IAppDbService
    {
        Task<string> GetShopAccessToken(string shopUrl);

        Task<int> CreateShopAccessToken(string accessToken, string shopUrl);

        Task<int> DeleteShopAccessToken(string shopUrl);

        //Task<BodiMeCredential> GetCredentials(string shopUrl);

        Task<string> GetScriptTagId(string shopUrl);

        Task<int> AddScriptTag(string shopUrl, string scriptTagId);

        Task<int> DeleteScriptTagId(string shopUrl);
    }
}
