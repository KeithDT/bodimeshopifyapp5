﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace BodiMeShopifyApp5.Services
{
    //Gives access to appsettings, without using dependency injection as required for the token service singleton
    public class AppSettings
    {
        #region Methods

        public static string GetSettingValue(string MainKey, string SubKey)
        {
            return Configuration.GetSection(MainKey).GetValue<string>(SubKey);
        }

        #endregion

        #region Properties

        public static IConfigurationRoot _configuration;
        public static IConfigurationRoot Configuration
        {
            get
            {
                if (_configuration == null)
                {
                    IConfigurationBuilder builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                            .AddEnvironmentVariables();
                    _configuration = builder.Build();
                }
                return _configuration;
            }
        }
        public static string DBConnection
        {
            get
            {
                return Configuration.GetSection("ConnectionStrings").GetValue<string>("ShopifyAppConnection");
            }
        }

        #endregion
    }
}
