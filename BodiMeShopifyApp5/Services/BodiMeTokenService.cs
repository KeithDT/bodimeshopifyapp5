﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BodiMeShopifyApp5.Data;
using BodiMeShopifyApp5.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.Text.Json;

namespace BodiMeShopifyApp5.Services
{
    public class BodiMeTokenService : IBodyMeTokenService
    {
        private readonly IMemoryCache _cache;

        public string BodiMeCoreUrl { get;}

        public BodiMeTokenService(IMemoryCache cache)
        {
            _cache = cache;
        }

        //Returns a cached token if it exists, otherwise initiates the call to get a new one from the bodimeAPI
        public async Task<BodiMeAPIToken> FetchToken(string shopUrl)
        {
            var logger = new DefaultLoggerService();

            BodiMeAPIToken bodiMeAPIToken = new BodiMeAPIToken();

            string cacheName = Constant.TokenEndPoint + "_" + shopUrl;

            logger.LogInfo("In memory cache looking for cacheName:" + cacheName);

            // if cache doesn't contain an entry called the cacheName
            // error handling mechanism is mandatory
            if (!_cache.TryGetValue(cacheName,  out bodiMeAPIToken))
            {
                var tokenmodel = await GetTokenFromApi(shopUrl);

                var options = new MemoryCacheEntryOptions();
                options.SetPriority(CacheItemPriority.NeverRemove);
                options.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(tokenmodel.SecondsItExpiresIn - 30); //take off 30 secs to ensure the token not expired when arriving at bodimecoreAPI
                options.RegisterPostEvictionCallback(MyCallback, this);

                _cache.Set(cacheName, tokenmodel, options);

                logger.LogInfo("In memory cache created cacheName:" + cacheName + " Timeout:" + TimeSpan.FromSeconds(tokenmodel.SecondsItExpiresIn - 30) + " Incoming secs:" + tokenmodel.SecondsItExpiresIn);

                bodiMeAPIToken = tokenmodel;
            }

            return bodiMeAPIToken;
        }

        //Call the bodimeAPI to get a new token
        private async Task<BodiMeAPIToken> GetTokenFromApi(string shopUrl)
        {
            var credential = await GetCredentials(shopUrl);

            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(credential.BodiMeAPI);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, credential.BodiMeAPI + Constant.TokenEndPoint)
            {
                Content = new StringContent($"grant_type={credential.GrantType}&username={credential.Username}&password={credential.Password}", Encoding.UTF8, "application/x-www-form-urlencoded")
            };
            requestMessage.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");

            HttpResponseMessage response = null;

            try
            {
                response = await httpClient.SendAsync(requestMessage);
            }
            //If the BodiMeAPI is down the response will be null, so catch the exception.
            catch (Exception e)
            {
                throw new Exception($"No success. Error code: {e.HResult}. {e.Message}");
            }

            if (response.IsSuccessStatusCode)
            {
                BodiMeAPIToken bodiMeAPIToken = new BodiMeAPIToken();
                using (var authTokenJSON = JsonDocument.Parse(await response.Content.ReadAsStringAsync()))
                {
                    var tokenRoot = authTokenJSON.RootElement;

                    bodiMeAPIToken.Value = tokenRoot.GetProperty("access_token").GetString();
                    bodiMeAPIToken.Type = tokenRoot.GetProperty("token_type").GetString();
                    bodiMeAPIToken.BodiMeAPI = credential.BodiMeAPI; //store the BodiMeAPI associated with the token, as it's always needed at the same place.
                    bodiMeAPIToken.SecondsItExpiresIn = tokenRoot.GetProperty("expires_in").GetInt32();
                }
                return bodiMeAPIToken;
            }
            throw new Exception($"No success. Error code: {response.StatusCode}. {response.ReasonPhrase}");
        }


        private async Task<BodiMeCredential> GetCredentials(string shopUrl)
        {
            //create a DbContext outside of dependency injection
            DbContextOptions contextOptions = new DbContextOptionsBuilder().
                UseSqlServer(AppSettings.DBConnection)
                .Options;

            var localContext = new AppDbContext(contextOptions);

            var credentials = await localContext.BodiMeCredentials.FirstOrDefaultAsync(d => d.ShopUrl == StandardizeUrl(shopUrl));
            if (credentials != null)
            {
                return credentials;
            }
            throw new Exception($"No credentials found for {shopUrl}.");
        }

        //Shop URLs in the database don't have the protocol, so check it is not contained in the incoming URL
        //and convert to lowercase
        public static string StandardizeUrl(string url)
        {
            if (url == null) return "";

            url = url.ToLower();

            string http = "http://";
            string https = "https://";

            int index = url.IndexOf(http, StringComparison.CurrentCultureIgnoreCase);
            string cleanPath = (index < 0)
                ? url
                : url.Remove(index, http.Length);

            index = url.IndexOf(https, StringComparison.CurrentCultureIgnoreCase);
            cleanPath = (index < 0)
                ? url
                : url.Remove(index, https.Length);

            return url;
        }

        private static void MyCallback(object key, object value, EvictionReason reason, object state)
        {
            var logger = new DefaultLoggerService();
            logger.LogInfo("Cache entry was removed :" +  reason);
        }
    }
}
