﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BodiMeShopifyApp5.Models;
using BodiMeShopifyApp5.Data;

namespace BodiMeShopifyApp5.Services
{
    public class InitService : IInitService
    {
        private readonly AppDbContext _context;

        public InitService(AppDbContext context)
        {
            _context = context;
        }

        //ToDo Support updates
        public async Task<int> CreateUpdateBodiMeCredentialsAsync(dynamic credentialData)
        {
            var credentials = new BodiMeCredential();
            credentials.GrantType = (string)credentialData.GrantType;
            credentials.Username = (string)credentialData.Username;
            credentials.Password = (string)credentialData.Password;
            credentials.ShopUrl = (string)credentialData.ShopUrl;
            credentials.BodiMeAPI = (string)credentialData.BodiMeAPI;

            _context.Add(credentials);

            var created = await _context.SaveChangesAsync();

            return created;
        }

        //ToDo Support updates
        public async Task<int> CreateUpdateShopTokenAsync(dynamic shopTokenData)
        {
            var shopToken = new ShopAccessToken();
            shopToken.AccessToken = (string)shopTokenData.AccessToken;
            shopToken.ShopUrl = (string)shopTokenData.ShopUrl;
            shopToken.ScriptTagId = (string)shopTokenData.ScriptTagId;

            _context.Add(shopToken);

            var created = await _context.SaveChangesAsync();

            return created;
        }
    }
}
