﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BodiMeShopifyApp5.Settings;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ShopifySharp;
using ShopifySharp.Filters;
using Microsoft.Rest;
using BodiMeShopifyApp5.Models;
using System.Net.Http.Json;
using BodiMeShopifyApp5.Infrastructure;

namespace BodiMeShopifyApp5.Services
{
    public class BodiMeCoreService : IBodiMeCoreService
    {
        private readonly IAppDbService _appDbService;

        private readonly IBodyMeTokenService _bodyMeTokenService;

        private readonly ILoggerManager _logger;

        public BodiMeCoreService(IAppDbService appDbService, ILoggerManager logger, IBodyMeTokenService bodyMeTokenService)
        {
            _appDbService = appDbService;
            _bodyMeTokenService = bodyMeTokenService;
            _logger = logger;
        }

        public async Task<HttpResponseMessage> GetSizeCharts(string shopUrl,string garmentId=null)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            HttpResponseMessage response;
            if (garmentId == null)
            {
                response = await client.GetAsync($"{client.BaseAddress}client/sizecharts");
            }
            else
            {
                response = await client.GetAsync($"{client.BaseAddress}client/sizecharts/{garmentId}");
            }
            
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;
        }

        public async Task<HttpResponseMessage> GetBrandsByGender(string shopUrl, int gender)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            HttpResponseMessage response;
            response = await client.GetAsync($"{client.BaseAddress}client/brands/{gender}");
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;
        }

        public async Task<HttpResponseMessage> GetBrandCategoriesByGender(string shopUrl,int brand_id, int gender)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            HttpResponseMessage response;
            response = await client.GetAsync($"{client.BaseAddress}garment/brandcategories/{brand_id}/{gender}");
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;
        }

        public async Task<HttpResponseMessage> GetBrandSizesByCategory(string shopUrl, int brand_id, int category_id)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            
            HttpResponseMessage response = await client.GetAsync($"{client.BaseAddress}garment/brandcategorysizes/{brand_id}/{category_id}");
            
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;
        }

        private async Task<HttpResponseMessage> HttpPost(string url, string requestBody, string shopUrl)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            var request = new StringContent(requestBody, Encoding.UTF8, "application/json");
            request.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var response = await client.PostAsync(url, request);
            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    return response;
                }
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;
        }

        //CREATE OR UPDATE BODY USING BRAND DATA
        public async Task<HttpResponseMessage> CreateUpdateBody(Models.BodyBrandMeasurement bodyMeasurementsBrand, string shopUrl)
        {
            var request = new StringContent(JsonConvert.SerializeObject(bodyMeasurementsBrand), Encoding.UTF8, "application/json");

            return await ExecuteNewBodyCall(request, shopUrl);
        }

        //CREATE OR UPDATE BODY USING GENERIC DATA
        public async Task<HttpResponseMessage> CreateUpdateBody(Dictionary<string, string> bodyMeasurementsMap, string shopUrl)
        {
            var serialisedbodyMeasurementsMap = JsonConvert.SerializeObject(bodyMeasurementsMap);

            var request = new StringContent(serialisedbodyMeasurementsMap, Encoding.UTF8, "application/json");

            return await ExecuteNewBodyCall(request, shopUrl);
        }

        //PERFORM THE CALL TO CREATE OR UPDATE A BODY
        public async Task<HttpResponseMessage> ExecuteNewBodyCall(StringContent request, string shopUrl)
        {
            _logger.LogInfo("BodiMeCoreService_ExecuteNewBodyCall:shop::" + shopUrl + ":request::" + request);

            var client = await GetHttpClientWithAuthentication(shopUrl);

            request.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

            var response = await client.PostAsync($"{client.BaseAddress}body/newBody", request);

            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    return response;
                }
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }

            return response;
        }

        //CREATE/UPDATE BODY WITH GENERIC SIZE CHART DATA AND RETURN SIZE ADVICE FOR THE BODY AND THE GARMENT
        public async Task<HttpResponseMessage> CreateUpdateBodyWithGenericSizeChartAndReturnSizeAdvice(Dictionary<string, string> bodyMeasurementsMap, string customerId, string garmentId, string shopUrl)
        {
            _logger.LogInfo("BodiMeCoreService_CreateUpdateBodyWithGenericSizeChartAndReturnSizeAdvice:shop::" + shopUrl + ":bodyMeasurementsMap::" + bodyMeasurementsMap + "garmentId::" + garmentId + "customerId::" + customerId);

            bodyMeasurementsMap.Add("customer_id", customerId);

            //Default to female if there is no gender (this was done for Curves which was only females).
            if (!bodyMeasurementsMap.ContainsKey("gender"))
            {
                bodyMeasurementsMap.Add("gender", "1");
            }

            //call newBody as this bodimecoreAPIhandles new bodies and updates so no requirement to check if the body already exists.
            var bodyResponse = await CreateUpdateBody(bodyMeasurementsMap, shopUrl);

            //success already checked in the new body, so just check if a bad request and return that if so
            if (bodyResponse.StatusCode == HttpStatusCode.BadRequest)
            {
                return bodyResponse;
            }

            //call size advice
            var suggestionResponse = await GetRecommendedSize(customerId, garmentId, shopUrl);
            return suggestionResponse;
        }

        //CALL SIZE ADVICE END POINT AND GET THE SUBSEQUENT SIZE ADVICE
        public async Task<HttpResponseMessage> GetRecommendedSize(string customerId, string garmentId, string shopUrl)
        {
            _logger.LogInfo("BodiMeCoreService_GetRecommendedSize:shop::" + shopUrl + "garmentId::" + garmentId + "customerId::" + customerId);

            // Get Size suggestion
            var client = await GetHttpClientWithAuthentication(shopUrl);
            var suggestionForm = new Dictionary<string, string> { { "yourbodyid", customerId }, { "yourgarmentid", garmentId } };
            var suggestionResponse = await HttpPost($"{client.BaseAddress}suggestion/newSuggestion", JsonConvert.SerializeObject(suggestionForm), shopUrl);
            if (suggestionResponse.StatusCode == HttpStatusCode.Created)
            {

                var response = await client.GetAsync(suggestionResponse.Headers.Location.AbsoluteUri);
                return response;
            }
            return suggestionResponse;
        }

        public async Task<HttpResponseMessage> DeleteBodyProfile(Models.BodyProfileDeleteCollection profiles,string customerId, string shopUrl)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            HttpRequestMessage request = new HttpRequestMessage
            {
                Content = new StringContent(JsonConvert.SerializeObject(profiles), Encoding.UTF8, "application/json"),
                Method = HttpMethod.Delete,
                RequestUri = new Uri($"{client.BaseAddress}body/profile/{customerId}")
            };
            var response = await client.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;

        }

        public async Task<HttpResponseMessage> GetBodyMeasurements(string customerId, string shopUrl)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            var response = await client.GetAsync($"{client.BaseAddress}body/{customerId}");
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;
        }

        public async Task<HttpResponseMessage> GetBodyProfile(string customerId, string shopUrl)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            var response = await client.GetAsync($"{client.BaseAddress}body/profile/{customerId}");
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;
        }

        public async Task<HttpResponseMessage> GetDefaultUnits(string shopUrl)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            var response = await client.GetAsync($"{client.BaseAddress}client");
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;
        }

        public async Task<HttpResponseMessage> SaveProfileMeasurements(Dictionary<string, string> bodyMeasurementsMap, string customerId, string shopUrl)
        {
            bodyMeasurementsMap.Add("customer_id", customerId);
            return await AmendBodyMeasurements(customerId, JsonConvert.SerializeObject(bodyMeasurementsMap), shopUrl);
        }

        public async Task<int> GetProductCount(string accessToken, string shopUrl)
        {
            var productService = new ProductService(shopUrl, accessToken);
            return await productService.CountAsync();
        }

        public async Task<int> SyncProducts(string accessToken, string shopUrl)
        {
            var productsCount = await GetProductCount(accessToken, shopUrl);
            var syncBatchId = Guid.NewGuid().ToString();
            var response = await SendProductsForSync(accessToken, shopUrl, productsCount, syncBatchId);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return productsCount;
        }

        public async Task<HttpResponseMessage> GetGarmentInformation(string urlPath, string shopUrl)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            var response = await client.GetAsync($"{client.BaseAddress}{urlPath}");
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;
        }

        private async Task<HttpResponseMessage> SendProductsForSync(string accessToken, string shopUrl, int productCount, string syncBatchId)
        {
            var productService = new ProductService(shopUrl, accessToken);
            var client = await GetHttpClientWithAuthentication(shopUrl);
            var productsList = await productService.ListAsync(new ProductListFilter { Limit = 250 });
            var allProducts = new List<Product>();
            while (true)
            {
                allProducts.AddRange(productsList.Items);

                if (!productsList.HasNextPage)
                {
                    break;
                }

                productsList = await productService.ListAsync(productsList.GetNextPageFilter());
            }
            var output = JsonConvert.SerializeObject(new { products = allProducts });
            var request = new StringContent(output, Encoding.UTF8, "application/json");
            request.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            request.Headers.Add("total-product-count", productCount.ToString());
            request.Headers.Add("sync-batch-id", syncBatchId);
            var response = await client.PutAsync($"{client.BaseAddress}garment/shopifygarmentssynch", request);
            return response;
        }

        // To get a recommendation, check if the body details for given customerId exists
        private async Task<bool> DoesBodyExists(string customerId, string shopUrl)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            var response = await client.GetAsync($"{client.BaseAddress}body/{customerId}");
            if (response.IsSuccessStatusCode) return true;
            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return false;
            }
            throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
        }

        private async Task<HttpResponseMessage> AmendBodyMeasurements(string customerId, string requestBody, string shopUrl)
        {
            var client = await GetHttpClientWithAuthentication(shopUrl);
            var request = new StringContent(requestBody, Encoding.UTF8, "application/json");
            request.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var response = await client.PutAsync($"{client.BaseAddress}body/amend/{customerId}", request);
            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return response; // according to this API behavior if no updates were made because the values were similar to existing values then it returns 404
                }
                throw new Exception($"No success. Error code: {response.StatusCode}. {response.AsFormattedString()}");
            }
            return response;
        }

        private async Task<HttpClient> GetHttpClientWithAuthentication(string shopUrl)
        {
            _logger.LogInfo("BodiMeCoreService_GetHttpClientWithAuthentication_FetchingToken");

            var bodimeAPIToken = await _bodyMeTokenService.FetchToken(shopUrl);

            _logger.LogInfo("BodiMeCoreService_GetHttpClientWithAuthentication_GotToken");

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(bodimeAPIToken.Type, bodimeAPIToken.Value);

            //set the base address in the client from the token
            client.BaseAddress = new Uri(bodimeAPIToken.BodiMeAPI);

            return client;
        }
    }
}
