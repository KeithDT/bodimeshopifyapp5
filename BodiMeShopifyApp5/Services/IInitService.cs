﻿using System.Threading.Tasks;
using BodiMeShopifyApp5.Models;

namespace BodiMeShopifyApp5.Services
{
    public interface IInitService
    {
        Task<int> CreateUpdateBodiMeCredentialsAsync(dynamic credentialData);

        Task<int> CreateUpdateShopTokenAsync(dynamic shopTokenData);
    }
}
