﻿using System;
using System.Threading.Tasks;
using BodiMeShopifyApp5.Data;
using BodiMeShopifyApp5.Infrastructure;
using BodiMeShopifyApp5.Models;
using Microsoft.EntityFrameworkCore;

namespace BodiMeShopifyApp5.Services
{
    public class AppDbService : IAppDbService
    {
        private readonly AppDbContext _context;
        private readonly ILoggerManager _logger;

        public AppDbService(AppDbContext context, ILoggerManager logger)
        {
            _context = context;
            _logger = logger;
        }

        //Assumes shop URLS in DB are in lower case
        //public async Task<BodiMeCredential> GetCredentials(string shopUrl)
        //{
        //    var credentials = await _context.BodiMeCredentials.FirstOrDefaultAsync(d => d.ShopUrl == StandardizeUrl(shopUrl));
        //    if (credentials != null)
        //    {
        //        return credentials;
        //    }
        //    throw new Exception($"No credentials found for {shopUrl}.");
        //}

        public async Task<string> GetShopAccessToken(string shopUrl)
        {
            _logger.LogInfo("AppDBServer_GetShopAccessToken_Shop:" + shopUrl);

            var shopAccessToken = await _context.ShopAccessTokens.FirstAsync(d => d.ShopUrl == StandardizeUrl(shopUrl));
            if (shopAccessToken != null) return shopAccessToken.AccessToken;
            else return "";
        }

        public async Task<string> GetScriptTagId(string shopUrl)
        {
            _logger.LogInfo("AppDBServer_GetScriptTagId_Shop:" + shopUrl);

            var shopAccessToken = await _context.ShopAccessTokens.FirstAsync(d => d.ShopUrl == StandardizeUrl(shopUrl));
            if (shopAccessToken != null) return shopAccessToken.ScriptTagId;
            else return "";
        }

        //sets the scripttadid to ""
        public async Task<int> DeleteScriptTagId(string shopUrl)
        {
            _logger.LogInfo("AppDBServer_GetScriptTagId_Shop:" + shopUrl);

            var shopAccessToken = await _context.ShopAccessTokens.FirstAsync(d => d.ShopUrl == StandardizeUrl(shopUrl));
            if (shopAccessToken != null)
            {
                shopAccessToken.ScriptTagId = "";
            }
            return await _context.SaveChangesAsync();
        }

        public async Task<int> AddScriptTag(string shopUrl, string scriptTagId)
        {
            _logger.LogInfo("AppDBServer_AddScriptTag_Shop:" + shopUrl + "::ScriptTagID:" + scriptTagId);

            var shopAccessToken = await _context.ShopAccessTokens.FirstAsync(d => d.ShopUrl == StandardizeUrl(shopUrl));
            shopAccessToken.ScriptTagId = scriptTagId;
            return await _context.SaveChangesAsync();
        }

        public async Task<int> CreateShopAccessToken(string accessToken, string shopUrl)
        {
            await DeleteShopAccessToken(shopUrl);// Delete existing shop token if any
            _context.Add(new ShopAccessToken { AccessToken = accessToken, ShopUrl = shopUrl });
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteShopAccessToken(string shopUrl)
        {
            var shopAccessToken = await _context.ShopAccessTokens.FirstOrDefaultAsync(d => d.ShopUrl == StandardizeUrl(shopUrl));
            if (shopAccessToken != null)
            {
                _context.ShopAccessTokens.Remove(shopAccessToken);
            }
            return await _context.SaveChangesAsync();
        }

        //Shop URLs in the database don't have the protocol, so check it is not contained in the incoming URL
        //and convert to lowercase
        public static string StandardizeUrl(string url)
        {
            if (url == null) return "";

            url = url.ToLower();

            string http = "http://";
            string https = "https://";

            int index = url.IndexOf(http, StringComparison.CurrentCultureIgnoreCase);
            string cleanPath = (index < 0)
                ? url
                : url.Remove(index, http.Length);

            index = url.IndexOf(https, StringComparison.CurrentCultureIgnoreCase);
            cleanPath = (index < 0)
                ? url
                : url.Remove(index, https.Length);

            return url;
        }
    }
}
