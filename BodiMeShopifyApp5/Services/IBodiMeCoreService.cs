﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BodiMeShopifyApp5.Services
{
    public interface IBodiMeCoreService
    {
        Task<int> SyncProducts(string accessToken, string shopUrl);
        Task<int> GetProductCount(string accessToken, string shopUrl);
        Task<HttpResponseMessage> GetSizeCharts(string shopUrl,string garmentId = null);
        Task<HttpResponseMessage> GetBrandsByGender(string shopUrl, int gender);
        Task<HttpResponseMessage> GetBodyProfile(string shopUrl, string customerId);
        Task<HttpResponseMessage> GetBrandCategoriesByGender(string shopUrl,int brand_id, int gender);
        Task<HttpResponseMessage> GetBrandSizesByCategory(string shopUrl, int brand_id, int category_id);
        Task<HttpResponseMessage> GetDefaultUnits(string shopUrl);
        Task<HttpResponseMessage> GetBodyMeasurements(string customerId, string shopUrl);
        Task<HttpResponseMessage> GetGarmentInformation(string urlPath, string shopUrl);
        Task<HttpResponseMessage> CreateUpdateBodyWithGenericSizeChartAndReturnSizeAdvice(Dictionary<string, string> bodyMeasurementsMap, string customerId, string garmentId, string shopUrl);
        Task<HttpResponseMessage> GetRecommendedSize(string customerId, string garmentId, string shopUrl);
        Task<HttpResponseMessage> CreateUpdateBody(Models.BodyBrandMeasurement bodyMeasurementsBrand, string shopUrl);
        Task<HttpResponseMessage> DeleteBodyProfile(Models.BodyProfileDeleteCollection profiles,string customerId,string shopUrl);
        Task<HttpResponseMessage> SaveProfileMeasurements(Dictionary<string, string> bodyMeasurementsMap, string customerId, string shopUrl);
    }
}
