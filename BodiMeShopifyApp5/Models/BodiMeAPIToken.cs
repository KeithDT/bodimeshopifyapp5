﻿namespace BodiMeShopifyApp5.Models
{
    public class BodiMeAPIToken
    {
        public string Value { get; set; }
        public string Type { get; set; }
        public int SecondsItExpiresIn { get; set; }
        public string BodiMeAPI { get; set; }
    }
}
