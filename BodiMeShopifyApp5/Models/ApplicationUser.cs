﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace BodiMeShopifyApp5.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

    }
}
