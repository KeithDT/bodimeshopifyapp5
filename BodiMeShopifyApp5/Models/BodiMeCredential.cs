﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BodiMeShopifyApp5.Models
{
    public class BodiMeCredential
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string GrantType { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ShopUrl { get; set; }
        public string BodiMeAPI { get; set; }
    }
}
