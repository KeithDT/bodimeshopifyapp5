﻿using BodiMeShopifyApp5.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BodiMeShopifyApp5.Models
{
    public class ApiError
    {
        private readonly ILoggerManager _logger;
        public ApiError(ILoggerManager logger)
        {
            _logger = logger;
        }

        public object ApiErrorMsg(string message)
        {
            Message = message;
            Detail = "";
            _logger.LogInfo("APIERROR_" + message);
            return this;
        }

        public object ApiErrorModelState(ModelStateDictionary modelState)
        {
            Message = "Invalid parameters.";
            Detail = modelState
                .FirstOrDefault(x => x.Value.Errors.Any()).Value.Errors
                .FirstOrDefault().ErrorMessage;
            return this;
        }

        public string Message { get; set; }

        public string Detail { get; set; }
    }
}
