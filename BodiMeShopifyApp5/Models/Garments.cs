﻿using System;
using System.Collections.Generic;
namespace BodiMeShopifyApp5.Models
{
    public class Garments
    {
        public string brandid { get; set; }
        public string categoryid { get; set; }
        public string sizeid { get; set; }
        public string fit { get; set; }
    }
}
