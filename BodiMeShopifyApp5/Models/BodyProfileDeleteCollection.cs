﻿using System;
using System.Collections.Generic;

namespace BodiMeShopifyApp5.Models
{
    public class BodyProfileDeleteCollection
    {
        public IList<BodyProfile> profiles { get; set; }
    }
}
