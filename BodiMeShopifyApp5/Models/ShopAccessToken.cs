﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BodiMeShopifyApp5.Models
{
    public class ShopAccessToken
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string ShopUrl { get; set; }
        public string AccessToken { get; set; }
        public string ScriptTagId { get; set; }

    }
}
