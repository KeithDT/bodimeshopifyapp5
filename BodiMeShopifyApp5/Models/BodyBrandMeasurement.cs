﻿using System;
using System.Collections.Generic;

namespace BodiMeShopifyApp5.Models
{
    public class BodyBrandMeasurement
    {
        public string height { get; set; }
        public string heightUnits { get; set; }      
        public string gender { get; set; }
        public string customer_id { get; set; }
        public Garments[] garments { get; set; }
    }
}
