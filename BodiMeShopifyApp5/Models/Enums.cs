﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BodiMeShopifyApp5.Models
{
    public class Constant
    {
        public static readonly string TokenEndPoint = "token"; //End point to get the bearer token
        public static readonly string CredentialCreationFailure = "Could not create or update credentials";
        public static readonly string ShopKeyCreationFailure = "Could not create or update shop key";
    }
}
