using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using BodiMeShopifyApp5.Data;
using BodiMeShopifyApp5.Services;
using BodiMeShopifyApp5.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using NLog;
using Microsoft.EntityFrameworkCore;
using BodiMeShopifyApp5.Models;
using Microsoft.AspNetCore.Identity;
using Quartz;
using static OpenIddict.Abstractions.OpenIddictConstants;
using Microsoft.AspNetCore.Mvc;
using BodiMeShopifyApp5.Infrastructure;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace BodiMeShopifyApp5
{
    public class Startup
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            LogManager.LoadConfiguration(String.Concat(hostingEnvironment.ContentRootPath, "/nlog.config"));
            Configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddSingleton<IBodyMeTokenService, BodiMeTokenService>();

            services.AddSingleton<ILoggerManager, DefaultLoggerService>();

            services.AddControllers();
            services.AddControllers().AddNewtonsoftJson();

            services.Configure<AppSetting>(Configuration);

            services.AddScoped<IAppDbService, AppDbService>();
            services.AddScoped<IBodiMeCoreService, BodiMeCoreService>();
            services.AddScoped<IInitService, InitService>();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAny",
                    policy => policy
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowAnyOrigin()
                    .WithExposedHeaders("Location")
                );
            });

            services.AddDbContext<AppDbContext>(options =>
            {
                // Configure the context to use Microsoft SQL Server.
                options.UseSqlServer(Configuration.GetConnectionString("ShopifyAppConnection"));

                // Register the entity sets needed by OpenIddict.
                // Note: use the generic overload if you need
                // to replace the default OpenIddict entities.
                options.UseOpenIddict();
            });

            // Register the Identity services.
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            // Configure Identity to use the same JWT claims as OpenIddict instead
            // of the legacy WS-Federation claims it uses by default (ClaimTypes),
            // which saves you from doing the mapping in your authorization controller.
            services.Configure<IdentityOptions>(options =>
            {
                options.ClaimsIdentity.UserNameClaimType = Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = Claims.Role;
            });

            // OpenIddict offers native integration with Quartz.NET to perform scheduled tasks
            // (like pruning orphaned authorizations/tokens from the database) at regular intervals.
            //Remove Quartz
            //services.AddQuartz(options =>
            //{
            //    options.UseMicrosoftDependencyInjectionJobFactory();
            //    options.UseSimpleTypeLoader();
            //    options.UseInMemoryStore();
            //});

            // Register the Quartz.NET service and configure it to block shutdown until jobs are complete.
            //Remove Quartz
            //services.AddQuartzHostedService(options => options.WaitForJobsToComplete = true);

            services.AddOpenIddict()

                // Register the OpenIddict core components.
                .AddCore(options =>
                {
                    // Configure OpenIddict to use the Entity Framework Core stores and models.
                    // Note: call ReplaceDefaultEntities() to replace the default OpenIddict entities.
                    options.UseEntityFrameworkCore()
                           .UseDbContext<AppDbContext>();

                    // Enable Quartz.NET integration.
                    //Remove Quartz
                    //options.UseQuartz();
                })

                // Register the OpenIddict server components.
                .AddServer(options =>
                {
                    // Enable the token endpoint.
                    options.SetTokenEndpointUris("/token");

                    // Enable the password flow.
                    options.AllowPasswordFlow();

                    // Accept anonymous clients (i.e clients that don't send a client_id).
                    options.AcceptAnonymousClients();

                    if (_hostingEnvironment.IsDevelopment())
                    {
                        // Register the signing and encryption credentials.
                        options.AddDevelopmentEncryptionCertificate()
                               .AddDevelopmentSigningCertificate();
                    }
                    else
                    {
                        options.AddSigningCertificate(new FileStream(_hostingEnvironment.ContentRootPath + "/certificates/openiddictcert.pfx", FileMode.Open), "bodibodi");

                        options.AddEncryptionCertificate(new FileStream(_hostingEnvironment.ContentRootPath + "/certificates/openiddictencryptcert.pfx", FileMode.Open), "bodibodi");
                    }

                    // Register the ASP.NET Core host and configure the ASP.NET Core-specific options.
                    options.UseAspNetCore()
                           .EnableTokenEndpointPassthrough();

                    options.SetAccessTokenLifetime(TimeSpan.FromHours(24));
                })

                // Register the OpenIddict validation components.
                .AddValidation(options =>
                {
                    // Import the configuration from the local OpenIddict server instance.
                    options.UseLocalServer();

                    // Register the ASP.NET Core host.
                    options.UseAspNetCore();
                });

            services
                .AddMvc(options =>
                {
                    options.CacheProfiles.Add("Static", new CacheProfile { Duration = 60, Location = ResponseCacheLocation.None, NoStore = true });
                    options.CacheProfiles.Add("Collection", new CacheProfile { Duration = 60, Location = ResponseCacheLocation.None, NoStore = true });
                    options.CacheProfiles.Add("Resource", new CacheProfile { Duration = 180, Location = ResponseCacheLocation.None, NoStore = true });

                    options.EnableEndpointRouting = false;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //Accept All HTTP Request Methods from all origins
            //Must be first, otherwise response header is not included for all types of response e.g. static files
            app.UseCors("AllowAny");

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
