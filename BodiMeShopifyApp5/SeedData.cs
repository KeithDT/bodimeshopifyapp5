﻿using BodiMeShopifyApp5.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BodiMeShopifyApp5
{
    public static class SeedData
    {
        public static async Task InitializeAsync(IServiceProvider services)
        {
            await AddAdminUserAndClient(
                services.GetRequiredService<UserManager<ApplicationUser>>()                );
        }

        private static async Task AddAdminUserAndClient(
            UserManager<ApplicationUser> userManager)
        {
            var dataExists = userManager.Users.Any();
            if (dataExists)
            {
                return;
            }


            // Add an admin user
            var user = new ApplicationUser
            {
                Email = "shopifyadmin@bodi.me",
                UserName = "shopifyadmin@bodi.me",
                FirstName = "Admin",
                LastName = "Administrator",
                CreatedAt = DateTimeOffset.UtcNow
            };

            var userCreated = await userManager.CreateAsync(user, "@up@nATimeInTheWest123!!");
            if (!userCreated.Succeeded)
                Console.WriteLine("User Creation Failed");

            // Put the user in the admin role
            await userManager.AddToRoleAsync(user, "Admin");
            await userManager.UpdateAsync(user);
        }
    }
}
