﻿using BodiMeShopifyApp5.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BodiMeShopifyApp5.Data
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext(DbContextOptions options)
            : base(options) { }

        public DbSet<ShopAccessToken> ShopAccessTokens { get; set; }

        public DbSet<BodiMeCredential> BodiMeCredentials { get; set; }
    }
}
